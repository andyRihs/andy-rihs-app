# andy's App

andy-rihs.ch mobile App
This App is used to demonstrate, how we could build an App based on a Homepage (like a PWA).
The App then allows to send messages (using oneSignal) to users with the App. 

## Getting Started with the Flutter-Project
Um das Projekt in einem Editor zu starten, muss Flutter installiert werden

- [Flutter installieren](https://flutter.dev/docs/get-started/install)
- [Editor aufsetzen](https://flutter.dev/docs/get-started/editor)
- [Introduction Video: Flutter macOS Setup 18:34, Flutter Windows Setup 41:32](https://www.youtube.com/watch?v=x0uinJvhNxI)

Ist Flutter installiert, kann das Projekt im Editor geöffnet werden. 
Als erstes müssen die Plugins geladen werden. Dazu im Projektordner im Terminal "flutter pub get" ausführen.

Bei geöffnetem Emulator oder erkanntem Smartphone kann der Code mit "flutter run" zum Leben erweckt werden. 

[Android Emulator verwenden: 1:26:52](https://www.youtube.com/watch?v=x0uinJvhNxI)

Um das Projekt via Editor auf einem iPhone oder iOS Emulator installieren zu können, wird XCode benötigt.
1. In Xcode, open Runner.xcworkspace in your app’s ios folder.
2. To view your app’s settings, select the Runner project in the Xcode project navigator. Then, in the main view sidebar, select the Runner target.
3. Select the General tab.

[Ganeze Anleitung hier](https://flutter.dev/docs/deployment/ios)

