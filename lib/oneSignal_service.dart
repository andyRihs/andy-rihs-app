
import 'package:onesignal_flutter/onesignal_flutter.dart';

class OneSignalService{
  //https://documentation.onesignal.com/docs/flutter-sdk-setup
  //Achtung: Diese Anleitung behandelt oneSignalSDK <3.0 ->
  //Example project https://pub.dev/packages/onesignal_flutter/versions/3.0.0-beta1
  //https://github.com/OneSignal/OneSignal-Flutter-SDK/tree/major-release-3.0.0
  static Future<void> initOneSignalPlatformState() async {
    //as in example project sdk >3.0
    print("init OneSignal");
    //if(mounted) return; //did return even if the app was completly reinstalled

    OneSignal.shared.setLogLevel(OSLogLevel.error, OSLogLevel.none);

    //as in exmaple project - needed if you like to wait until user accept privacy paper
    //await OneSignal.shared.setRequiresUserPrivacyConsent(_requireConsent);

    await OneSignal.shared.consentGranted(true);
    print("consent granted");

    //ToDo: implement as in example if needed
    //OneSignal.shared.setNotificationOpenedHandler((openedResult) { })

    //replace for setInFocusDisplayType
    OneSignal.shared
        .setNotificationWillShowInForegroundHandler((OSNotificationReceivedEvent event) {
      print("Foreground handle called with: ${event.notification.body}");
      event.complete(null);
      //this.setState(() {
      //  //toDo set a notificaiton on the screen
      //});
    });

    print("vor setAppID");
    //https://documentation.onesignal.com/docs/step-by-step-flutter-2x-to-300-upgrade-guide
    await OneSignal.shared.setAppId("a74b77f0-8d1a-4aff-aa0b-cf081714d206");
    print("nach setAppID");

    //OneSignal.shared.disablePush(false);

    // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt.
    // We recommend removing the following code and instead using an In-App Message to prompt for notification permission
    //https://documentation.onesignal.com/docs/ios-push-opt-in-prompt
    //await OneSignal.shared.promptUserForPushNotificationPermission(fallbackToSettings: true);

    OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      // will be called whenever a notification is received
      print("user tabed notification " + result.notification.body);
    });

    //will be triggert by in App message and by OS permission request.
    OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
      print("permission changed");
    });

    //triggert with in App message
    OneSignal.shared.setSubscriptionObserver((OSSubscriptionStateChanges changes) {
      print("subscritption changed");
    });

    //https://documentation.onesignal.com/docs/ios-push-opt-in-prompt
    /*
    (Pre-major release documentation)
    You should use the `getPermissionSubscriptionState` [method](doc:sdk-reference#getpermissionsubscriptionstate-method) to detect if the device is subscribed or not.
    If the device is not subscribed, simply use the `addTrigger` [method](doc:sdk-reference#in-app-messages) to show the In-App Message Prompt for iOS.

    Make sure the trigger "key" and "value" match what you added to the IAM Trigger. In this example, we used `prompt_ios` for the key and `true` for the value.
    */
    //var status = await OneSignal.shared.getDeviceState().;

    //if !(status.permissionStatus.hasPrompted)
    //OneSignal.addTrigger("prompt_ios", "true");
  }

}

