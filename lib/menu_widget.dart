import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MenuWidget extends StatelessWidget {
  MenuWidget(this._webViewControllerFuture);
  final Future<WebViewController> _webViewControllerFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _webViewControllerFuture,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> controller) {
        if (!controller.hasData) return Container();
        return PopupMenuButton<String>(
          tooltip: 'menu',
          offset: Offset(0, 20),
          onSelected: (String value) {
            if (value == 'Email link') {
              launch(Uri(
                  scheme: 'mailto',
                  path: 'info@andy-rihs.ch',
                  queryParameters: {'subject': 'Question regarding '})
                  .toString());
            } else if (value == 'Phone call') {
              launch(Uri(
                scheme: 'tel',
                path: '+41 79 745 24 18',
              ).toString());
            } else if(value == 'Message'){
              //ToDo: show promt to opt in or out of messaging service
            }
          },
          itemBuilder: (BuildContext context) => <PopupMenuItem<String>>[
            const PopupMenuItem<String>(
              value: 'Email link',
              child: Text('E-Mail me'),
            ),
            const PopupMenuItem<String>(
              value: 'Phone call',
              child: Text('Call me'),
            ),
            const PopupMenuItem<String>(
              child: Text('Messages'),
              value: 'message',
            ),
          ],
        );
      },
    );
  }
}