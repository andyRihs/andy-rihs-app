import 'dart:async';
import 'dart:io';

import 'package:andy_rihs_app/oneSignal_service.dart';
import 'package:andy_rihs_app/refresh_page_widget.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'menu_widget.dart';
import 'navigation_control_widget.dart';

void main() {
  runApp(MyHomePageApp());
}

class MyHomePageApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Andy's Homepage",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Completer<WebViewController> _controller = Completer<WebViewController>();
  int positionIndex = 0;

  @override
  void initState() {
    super.initState();
    //no idea wath this is for? ria 21.03.2021
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }

    OneSignalService.initOneSignalPlatformState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Andy's App"),
        actions: [
          NavigationControlsWidget(_controller.future),
          RefreshPageWidget(_controller.future),
          MenuWidget(_controller.future),
        ],
      ),
      body: IndexedStack(
        index: positionIndex,
        children: <Widget>[
          WebView(
            initialUrl: "https://andy-rihs.ch/",
            //javascript must be enabled to run the menu
            javascriptMode: JavascriptMode.unrestricted,
            //https://medium.com/@manikandan_selvanathan/flutter-webview-url-handling-a9bb1abfa8bd
            navigationDelegate: (navigation) {
              if (navigation.isForMainFrame) {
                return NavigationDecision.navigate;
              } else {
                return NavigationDecision.prevent;
              }
            },
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
              //webViewController.reload();
              print("Wb View created");
            },
            onPageStarted: (url) {
              setState(() {
                positionIndex = 1;
              });

              print("page load started");
            },
            onPageFinished: (url) {
              setState(() {
                positionIndex = 0;
              });

              print("page loaded");
            },
          ),
          Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
        ],
      ),
    );
  }
}
