import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class RefreshPageWidget extends StatelessWidget {
  const RefreshPageWidget(this._webViewControllerFuture)
      : assert(_webViewControllerFuture != null);

  final Future<WebViewController> _webViewControllerFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: _webViewControllerFuture,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> snapshot) {
        final bool webViewReady =
            snapshot.connectionState == ConnectionState.done;
        final WebViewController controller = snapshot.data;
        return IconButton(
          icon: Icon(Icons.refresh_rounded),
          onPressed: webViewReady ? () => controller.reload() : null,
        );
      },
    );
  }
}