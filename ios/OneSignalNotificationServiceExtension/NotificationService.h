//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Andy Rihs on 24.03.21.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
